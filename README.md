# Stock Notifier

Service de notification de retour de stock pour les solutions e-commerce

## Lancer le service
En production
```bash
node ace build --production
cp .env build/
cd build
npm ci --production
node server.js
```
En développement
```bash
node ace serve --watch
```