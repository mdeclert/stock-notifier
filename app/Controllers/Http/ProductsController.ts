import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Product from "App/Models/Product";

export default class ProductsController {
  public async listProducts({ view }: HttpContextContract) {
    const products = await Product.all()
    return await view.render('home', { products })
  }
}
