import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Subscriber from 'App/Models/Subscriber'

export default class NotificationsController {
  public async subscribeCustomer({ request }: HttpContextContract) {
    try {
      const { email, productId } = request.all()
      const subscriber = await Subscriber.query()
        .where('email', 'LIKE', email)
        .where('product_id', '=', productId)
        .first()

      if (subscriber !== null)
        throw 'You already subscribed to restock notification for this product'

      await Subscriber.create({ email, productId })
      return {
        success: true,
        message: "Successfully subscribed to restock notification for this product"
      }
    } catch (error) {
      return {
        success: false,
        message: error
      }
    }
  }
}
