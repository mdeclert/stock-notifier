import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Product from 'App/Models/Product'

export default class ProductSeeder extends BaseSeeder {
  public async run () {
    await Product.createMany([
      {
        shopifyId: '788032119674292922',
        title: 'Example T-Shirt',
        vendor: 'Acme',
        handle: 'example-t-shirt',
        inventoryQuantity: 0,
        tags: 'example, mens, t-shirt'
      },
      {
        shopifyId: '787032759674192623',
        title: 'Example T-Shirt 2',
        vendor: 'Acme',
        handle: 'example-t-shirt-2',
        inventoryQuantity: 75,
        tags: 'example, mens, t-shirt'
      },
      {
        shopifyId: '781034119674275931',
        title: 'Example T-Shirt 3',
        vendor: 'Acme',
        handle: 'example-t-shirt-3',
        inventoryQuantity: 75,
        tags: 'example, mens, t-shirt'
      },
      {
        shopifyId: '988632419774692421',
        title: 'Example T-Shirt 4',
        vendor: 'Acme',
        handle: 'example-t-shirt-4',
        inventoryQuantity: 75,
        tags: 'example, mens, t-shirt'
      }
    ])
  }
}
