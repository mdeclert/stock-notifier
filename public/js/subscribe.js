import FetchRequest from "./classes/fetch.class.js";

const subscribeForm = document.querySelector('#notification-subscribe')
subscribeForm.addEventListener('submit', async event => {
  event.preventDefault()

  const productId = event.target.querySelector('input#product-id').value
  const email = event.target.querySelector('input#email').value
  const request = new FetchRequest('subscribe-notification', 'POST', {
    productId,
    email
  })

  try {
    const response = await request.send()
    console.log(response)
  } catch (error) {
    console.error(error)
  }
})
