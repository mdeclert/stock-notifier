export default class FetchRequest {
  constructor(endpoint, method = 'GET', data = null) {
    this.url = `${location.protocol}//${location.host}/${endpoint}`
    this.method = method
    this.data = data
  }

  async send() {
    const options = {
      method: this.method,
      mode: 'cors'
    }

    if (this.method !== 'GET' && this.method !== 'OPTIONS') {
      options.body = JSON.stringify(this.data)
      options.headers = { 'Content-Type': 'application/json' }
    }

    return fetch(this.url, options)
      .then(async response => {
        if (response.ok)
          return await response.json()

        throw new Error(`An error occured: ${response.status}, ${response.statusText}`)
      })
  }
}